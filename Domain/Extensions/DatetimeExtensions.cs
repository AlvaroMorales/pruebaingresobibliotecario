﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Extensions
{
    public static class DatetimeExtensions
    {
        public static DateTime AddDaysWithoutWeekends(this DateTime value, int days)
        {
            while (days > 0)
            {
                value = value.AddDays(1);
                if (value.DayOfWeek != DayOfWeek.Saturday && value.DayOfWeek != DayOfWeek.Sunday)
                {
                    days--;
                }
            }
            return value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum UserType
    {
        Afiliado = 1,
        Empleado = 2,
        Invitado = 3
    }
}

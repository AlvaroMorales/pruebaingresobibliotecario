﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum StatusReponse
    {
        Ok,
        NotFound,
        NotProcessed,
        ClientError,
        ServerError
    }
}

﻿using Domain.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class InsertedPrestamoDTO : GenericResponse
    {
        public string id { get; set; }
        public string fechaMaximaDevolucion { get; set; }
    }
}

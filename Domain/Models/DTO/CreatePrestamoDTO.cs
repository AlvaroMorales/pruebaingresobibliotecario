﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class CreatePrestamoDTO
    {
        public string isbn { get; set; }
        public string identificacionUsuario { get; set; }
        public int tipoUsuario { get; set; }
    }
}

﻿using Domain.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class PrestamoDTO : GenericResponse
    {
        public string id { get; set; }
        public Guid isbn { get; set; }
        public string identificacionUsuario { get; set; }
        public byte tipoUsuario { get; set; }
        public string fechaMaximaDevolucion { get; set; }
    }
}

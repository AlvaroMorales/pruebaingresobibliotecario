﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Entities
{
    public class Prestamo
    {
        public Guid id { get; set; }
        public Guid isbn { get; set; }
        public string identificacionUsuario { get; set; }
        public byte tipoUsuario { get; set; }
        public DateTime fechaMaximaDevolucion { get; set; }
    }
}

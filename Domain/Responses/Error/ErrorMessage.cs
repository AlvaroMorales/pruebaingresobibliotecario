﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Responses.Error
{
    public class ErrorMessage : GenericResponse
    {
        public string mensaje { get; set; }
    }
}

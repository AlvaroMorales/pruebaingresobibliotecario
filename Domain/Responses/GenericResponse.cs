﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Domain.Responses
{

    public abstract class GenericResponse
    {
        [JsonIgnore]
        public StatusReponse Control { get; set; }

    }
}

﻿using Application.IBusinessLogic;
using Application.Validators;
using AutoMapper;
using Domain.Enums;
using Domain.Models.DTO;
using Domain.Models.Entities;
using Domain.Repository;
using Domain.Responses;
using Domain.Responses.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.BusinessLogic
{
    public class PrestamoBL : IPrestamoBL
    {
        private readonly IRepository<Prestamo> _repository;
        private readonly IMapper _mapper;

        public PrestamoBL(IRepository<Prestamo> repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }
        /// <summary>
        /// Create a new prestamo <see cref="PrestamoDTO"/>.
        /// </summary>
        /// <param name="prestamo">Prestamo to create.</param>
        /// <returns>An object response.</returns>
        public async Task<GenericResponse> CreatePrestamo(CreatePrestamoDTO prestamo)
        {
            try
            {
                if (!Guid.TryParse(prestamo.isbn, out var isbn))
                {
                    return new ErrorMessage { mensaje = "Debe ingresar un isbn válido.", Control = StatusReponse.ClientError };
                }
                if (prestamo.identificacionUsuario.Length > 10)
                {
                    return new ErrorMessage { mensaje = "Debe ingresar una identificación de usuario válida.", Control = StatusReponse.ClientError };
                }
                bool validUserType = false;
                foreach (var item in Enum.GetValues(typeof(UserType)))
                {
                    if (item.GetHashCode() == prestamo.tipoUsuario)
                    {
                        validUserType = true;
                        break;
                    }
                }
                if (!validUserType)
                {
                    return new ErrorMessage { mensaje = "Debe registrar un tipo de usuario válido.", Control = StatusReponse.ClientError };
                }
                var userExists = await _repository.Get(x => x.identificacionUsuario == prestamo.identificacionUsuario);
                if (userExists != null && userExists.Any())
                {
                    return new ErrorMessage { mensaje = $"El usuario con identificacion {prestamo.identificacionUsuario} ya tiene un libro prestado por lo cual no se le puede realizar otro prestamo", Control = StatusReponse.ClientError };
                }
                var model = _mapper.Map<Prestamo>(prestamo);
                IUserTypeFechaEntrega fechaEntrega = prestamo.tipoUsuario switch
                {
                    ((int)UserType.Afiliado) => new FechaEntregaAfiliado(),
                    ((int)UserType.Empleado) => new FechaEntregaEmpleado(),
                    _ => new FechaEntregaInvitado(),
                };
                model.fechaMaximaDevolucion = fechaEntrega.CalculateFechaEntrega();
                bool inserted = await _repository.Create(model);
                if (inserted)
                {
                    return _mapper.Map<InsertedPrestamoDTO>(model);
                }
                return new ErrorMessage { mensaje = "Ha ocurrido un error al registrar el préstamo.", Control = StatusReponse.ServerError };
            }
            catch (Exception ex)
            {
                _ = ex;
                return new ErrorMessage { mensaje = "Ha ocurrido un error al registrar el préstamo.", Control = StatusReponse.ServerError };
            }
        }
        /// <summary>
        /// Retrive an especific prestamo <see cref="PrestamoDTO"/>.
        /// </summary>
        /// <returns>An object response.</returns>
        public async Task<GenericResponse> GetPrestamo(string id)
        {
            try
            {
                var exists = await _repository.Get(x => x.id.ToString().Equals(id));
                if (exists != null && exists.Any())
                {
                    return _mapper.Map<PrestamoDTO>(exists.FirstOrDefault());
                }
                else
                {
                    return new ErrorMessage { mensaje = $"El prestamo con id {id} no existe", Control = StatusReponse.NotFound };
                }
            }
            catch (Exception ex)
            {
                _ = ex;
                return new ErrorMessage { mensaje = "Ha ocurrido un error al registrar el préstamo.", Control = StatusReponse.ServerError };
            }
        }
    }
}



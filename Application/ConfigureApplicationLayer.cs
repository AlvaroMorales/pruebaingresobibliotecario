﻿using Application.BusinessLogic;
using Application.IBusinessLogic;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace Application
{
    public static class ConfigureApplicationLayer
    {
        /// <summary>
        /// Services layer for application layer.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddScoped<IPrestamoBL, PrestamoBL>();

            return services;
        }
    }
}

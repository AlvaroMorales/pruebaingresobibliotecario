﻿using Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Validators
{
    public class FechaEntregaEmpleado : IUserTypeFechaEntrega
    {
        /// <summary>
        /// Calculate the date regarding to the usertype.
        /// </summary>
        /// <returns>Datetime.</returns>
        public DateTime CalculateFechaEntrega()
        {
            return DateTime.Today.AddDaysWithoutWeekends(8);
        }
    }
}

﻿using Domain.Models.DTO;
using Domain.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.IBusinessLogic
{
    public interface IPrestamoBL
    {
        /// <summary>
        /// Retrive an especific prestamo <see cref="PrestamoDTO"/>.
        /// </summary>
        /// <returns>An object response.</returns>
        Task<GenericResponse> GetPrestamo(string id);

        /// <summary>
        /// Create a new prestamo <see cref="PrestamoDTO"/>.
        /// </summary>
        /// <param name="prestamo">Prestamo to create.</param>
        /// <returns>An object response.</returns>
        Task<GenericResponse> CreatePrestamo(CreatePrestamoDTO prestamo);
    }
}

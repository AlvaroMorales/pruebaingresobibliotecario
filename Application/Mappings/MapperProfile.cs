﻿using AutoMapper;
using Domain.Models.DTO;
using Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Mappings
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Prestamo, PrestamoDTO>().ReverseMap();
            CreateMap<Prestamo, CreatePrestamoDTO>().ReverseMap();
            CreateMap<Prestamo, InsertedPrestamoDTO>()
                .ForMember(x => x.fechaMaximaDevolucion, y => y.MapFrom(src => src.fechaMaximaDevolucion.ToShortDateString()))
                .ReverseMap();

        }
    }
}

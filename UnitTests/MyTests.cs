using Application.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    [TestClass]
    public class MyTests
    {
        [TestMethod]
        public void ShouldCalculateMyDateAfiliado()
        {
            DateTime expectedDate = new DateTime(2022, 02, 23);

            IUserTypeFechaEntrega calculator = new FechaEntregaAfiliado();
            DateTime result = calculator.CalculateFechaEntrega();

            Assert.AreEqual(expectedDate, result);
        }

        [TestMethod]
        public void ShouldCalculateMyDateEmpleado()
        {
            DateTime expectedDate = new DateTime(2022, 02, 21);

            IUserTypeFechaEntrega calculator = new FechaEntregaEmpleado();
            DateTime result = calculator.CalculateFechaEntrega();

            Assert.AreEqual(expectedDate, result);
        }

        [TestMethod]
        public void ShouldCalculateMyDateInvitado()
        {
            DateTime expectedDate = new DateTime(2022, 02, 18);

            IUserTypeFechaEntrega calculator = new FechaEntregaInvitado();
            DateTime result = calculator.CalculateFechaEntrega();

            Assert.AreEqual(expectedDate, result);
        }
    }
}

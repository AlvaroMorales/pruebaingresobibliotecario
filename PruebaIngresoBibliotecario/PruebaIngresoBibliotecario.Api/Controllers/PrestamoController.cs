﻿using Application.IBusinessLogic;
using Domain.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using PruebaIngresoBibliotecario.Api.Response;
using System.Threading.Tasks;

namespace PruebaIngresoBibliotecario.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrestamoController : ResponseHandler
    {
        private readonly IPrestamoBL _prestamoBL;

        public PrestamoController(IPrestamoBL prestamoBL)
        {
            this._prestamoBL = prestamoBL;
        }
        [HttpPost]
        public async Task<IActionResult> Post(CreatePrestamoDTO prestamo)
        {
            var result = await _prestamoBL.CreatePrestamo(prestamo);
            return ProcessResponse(result);
        }
        [HttpGet("{idPrestamo}")]
        public async Task<IActionResult> Get(string idPrestamo)
        {
            var result = await _prestamoBL.GetPrestamo(idPrestamo);
            return ProcessResponse(result);
        }
    }
}

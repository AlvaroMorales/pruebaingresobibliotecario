﻿using Domain.Enums;
using Domain.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIngresoBibliotecario.Api.Response
{
    public abstract class ResponseHandler : ControllerBase
    {
        protected IActionResult ProcessResponse(GenericResponse response)
        {
            return response.Control switch
            {
                StatusReponse.Ok => Ok(response),
                StatusReponse.NotFound => NotFound(response),
                StatusReponse.ClientError => BadRequest(response),
                _ => BadRequest(response),
            };
        }
    }
}

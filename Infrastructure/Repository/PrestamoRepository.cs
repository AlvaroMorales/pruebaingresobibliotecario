﻿using Domain.Models.Entities;
using Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class PrestamoRepository : IRepository<Prestamo>
    {
        private readonly PersistenceContext _dbContext;

        public PrestamoRepository(PersistenceContext dBContext) => this._dbContext = dBContext;
        public async Task<bool> Create(Prestamo prestamo)
        {
            await _dbContext.Prestamo.AddAsync(prestamo);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> Delete(Prestamo prestamo)
        {
            _dbContext.Prestamo.Remove(prestamo);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<List<Prestamo>> Get(Expression<Func<Prestamo, bool>> where = null)
        {
            IQueryable<Prestamo> query = _dbContext.Set<Prestamo>();

            if (where != null)
            {
                query = query.Where(where);
            }
            return await query.ToListAsync();
        }

        public async Task<bool> Update(Prestamo prestamo)
        {
            _dbContext.Prestamo.Update(prestamo);
            return await _dbContext.SaveChangesAsync() > 1;
        }
    }
}

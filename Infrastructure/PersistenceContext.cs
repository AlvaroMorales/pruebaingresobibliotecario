﻿using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class PersistenceContext : DbContext
    {

        private readonly IConfiguration Config;

        public PersistenceContext(DbContextOptions<PersistenceContext> options, IConfiguration config) : base(options)
        {
            Config = config;
        }

        public async Task CommitAsync()
        {
            await SaveChangesAsync().ConfigureAwait(false);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Prestamo>(entity =>
            {
                entity.ToTable("Prestamo");
                entity.HasKey(x => x.id);
                entity.Property(x => x.id).HasColumnName("id");
                entity.Property(x => x.id).HasDefaultValueSql("NEWID()");

                entity.Property(x => x.isbn)
               .HasColumnName("isbn")
               .IsRequired();

                entity.Property(x => x.identificacionUsuario)
                   .HasColumnName("Identificacion_usuario")
                   .IsRequired()
                   .HasMaxLength(10)
                   .IsUnicode(false);

                entity.Property(x => x.tipoUsuario)
                   .HasColumnName("Tipo_usuario")
                   .IsRequired();

                entity.Property(x => x.fechaMaximaDevolucion)
                   .HasColumnName("Fecha_maxima_devolucion")
                   .IsRequired();

                entity.HasIndex(x => x.identificacionUsuario);

            });

            base.OnModelCreating(modelBuilder);
        }
        public virtual DbSet<Prestamo> Prestamo { get; set; }
    }
}
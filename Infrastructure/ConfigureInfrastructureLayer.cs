﻿using Domain.Models.Entities;
using Domain.Repository;
using Infrastructure.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public static class ConfigureInfrastructureLayer
    {
        /// <summary>
        /// Services layer for application layer.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddInfraestructure(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddScoped<IRepository<Prestamo>, PrestamoRepository>();

            services.AddDbContext<PersistenceContext>(option =>
            option.UseInMemoryDatabase(configuration.GetConnectionString("database")));

            return services;
        }
    }
}
